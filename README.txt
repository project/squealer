Drush 9 command used to query a Drupal 7 site database and generate an Excel file that can be used to identify migration data inconsistencies and test cases.

# How to get started
---------------------

1. Initialize a new Drupal 8 site, and ensure the Drupal 7 database is registered in the settings.php file as "drupal_7".
2. Add the module to composer by running `composer require drupal/squealer`.
3. Enable the module using drush by running `drush en squealer`.

# How to use
-------------

1. Run the drush command `drush squealer:squeal`.
2. Go the site's files directory, within the "squealer_reports" directory to find the generated Excel file.
