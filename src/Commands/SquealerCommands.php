<?php

namespace Drupal\squealer\Commands;

use Drush\Commands\DrushCommands;
use Drush\Commands\AcquiaDisallowedCommandException;
use Drupal\Core\DrupalKernel;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\File\FileSystem;
use Drush\Log\LogLevel;
use Georgia\Env;
use Drupal\Core\Database\Query\Condition;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class SquealerCommands extends DrushCommands {

  /**
   * Variable to store the Drupal 7 database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Site path.
   *
   * @var string
   */
  protected $sitePath;

  /**
   * Site alias domain.
   *
   * @var string
   */
  protected $siteDomain;

  /**
   * Site alias domain URL.
   *
   * @var string
   */
  protected $siteDomainUrl;

  /**
   * String queries.
   *
   * A multidemensional array of string queries including a label and a list
   * of strings for the query.
   *
   * @var array
   */
  protected $stringQueries;

  /**
   * String queries field names.
   *
   * A multidimensional array of fields to be queried by string queries. If
   * tables need to be joined to identify the node, they can be listed in a
   * join_tables attribute, including the field_name, table prefix and the
   * to/from entity types.
   *
   * @var array
   */
  protected $stringQueriesFieldNames;

  /**
   * String queries table prefixes.
   *
   * Table prefixes for data and revision tables that can be added to the field
   * names for queries.
   *
   * @var array
   */
  protected $stringQueriesTablePrefixes;

  /**
   * Content date queries.
   *
   * A multidimensional array including a label, content_type, date column, date
   * and the date operator parameters for specific date queries.
   *
   * @var array
   */
  protected $contentDateQueries;

  /**
   * Date columns.
   *
   * An array of date column names used for identifying the correct column for
   * each query.
   *
   * @var array
   */
  protected $dateColumns;

  /**
   * Paragraphs bundles.
   *
   * A multidimensional array of paragraphs bundles with the fields.
   *
   * @var array
   */
  protected $paragraphsBundles;

  /**
   * Paragraphs query field names.
   *
   * A multidimensional array of fields to be queried by the paragraphs query.
   * If tables need to be joined to identify the node, they can be listed in a
   * join_tables attribute, including the field_name, table prefix and the
   * to/from entity types.
   *
   * @var array
   */
  protected $paragraphsQueryFieldNames;

  /**
   * Metatag queries.
   *
   * A multidimensional array of metatag queries including a label and a list
   * of strings for the query.
   *
   * @var array
   */
  protected $metatagQueries;

  /**
   * Field data queries.
   *
   * A multidimensional array of field data queries including a label, and a
   * list of the field value column names.
   *
   * @var array
   */
  protected $fieldDataQueries = [];

  /**
   * Recursive table join node prefixes for formatting.
   *
   * An array of prefixes to identify node table joins and data. When the node
   * table is joined multiple times to the same query, each alias is prefixed.
   * The prefix is necessary when identifying data in the formatResults method.
   *
   * @var array
   */
  protected $recursiveNodePrefixes = [];

  /**
   * Array of scan results.
   *
   * An array that stores the results from each query so that reports can be
   * consolidated and more useful.
   *
   * @var array
   */
  protected $reports;

  /**
   * Array of scans to run.
   *
   * An array of scan ids that can be specified when running the drush command
   * so that specific scans can run instead of all.
   *
   * @var array
   */
  protected $scans;

  /**
   * Array of export formats.
   *
   * An array of file formats for exported data.
   *
   * @var array
   */
  protected $formats;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The drupal kernel service.
   *
   * @var \Drupal\Core\DrupalKernel
   */
  protected $drupalKernel;

  /**
   * Constructs a new DrushCommands object.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The file system service.
   * @param \Drupal\Core\DrupalKernel $drupal_kernel
   *   The drupal kernel service.
   */
  public function __construct(
    DateFormatter $date_formatter,
    FileSystem $file_system,
    DrupalKernel $drupal_kernel
  ) {
    $this->dateFormatter = $date_formatter;
    $this->fileSystem = $file_system;
    $this->drupalKernel = $drupal_kernel;
    // Set the current site domain.
    $this->siteDomain = Env::getD7SiteDomain();
    // Set the current site path.
    $this->sitePath = $this->siteDomain . '/';
    // Set the current site domain for URL alias links based on the site path.
    $this->siteDomainUrl = 'https://' . $this->sitePath;
    // Initialize reports.
    $this->reports = [];
    // Scans to run.
    $this->scans = [
      'strq',
      'dateq',
      'parent',
      'unpub',
      'docs',
      'paragraphs',
      'field_collections',
      'metaq',
      'redirects',
      'fieldq',
    ];
    // Formats for export.
    $this->formats = [
      'csv',
      'excel',
    ];
    // Set the string queries with parameters.
    $this->stringQueries = [
      'objects' => [
        'label' => 'WYSIWYG Object',
        'strings' => [
          '%<object%',
        ],
      ],
      'images' => [
        'label' => 'WYSIWYG Image',
        'strings' => [
          '%<img%',
        ],
      ],
      'embedded_entities' => [
        'label' => 'WYSIWYG Embedded Entity',
        'strings' => [
          '%<div class="drupal-embed" embed_type="node" nid="%" view_mode="%"></div>%',
        ],
      ],
      'absolute_links' => [
        'label' => 'WYSIWYG Absolute Links',
        'strings' => [
          "%http%://%$this->siteDomain%",
        ],
      ],
      'spans' => [
        'label' => 'WYSIWYG Span',
        'strings' => [
          '%<span%',
        ],
      ],
      'iframes' => [
        'label' => 'WYSIWYG iFrame',
        'strings' => [
          '%<iframe%',
        ],
      ],
      'scripts' => [
        'label' => 'WYSIWYG Script',
        'strings' => [
          '%<script%',
        ],
      ],
      'tables' => [
        'label' => 'WYSIWYG Table',
        'strings' => [
          '%<table%',
        ],
      ],
      'custom_classes' => [
        'label' => 'WYSIWYG Custom Class',
        'strings' => [
          '% class="%',
        ],
      ],
      'custom_forms' => [
        'label' => 'WYSIWYG Custom Form',
        'strings' => [
          '%<form%',
        ],
      ],
      'email_links' => [
        'label' => 'WYSIWYG Email Link',
        'strings' => [
          '%href="mailto:%',
        ],
      ],
      'meta_tag' => [
        'label' => 'WYSIWYG Meta Tag',
        'strings' => [
          '%<meta%',
        ],
      ],
      'styles' => [
        'label' => 'WYSIWYG Style',
        'strings' => [
          '%<style%',
          '% style="%',
        ],
      ],
      'social_networks' => [
        'label' => 'WYSIWYG Social Network',
        'strings' => [
          '%facebook.com%',
          '%twitter.com%',
          '%instagram.com%',
          '%linkedin.com%',
          '%pinterest.com%',
          '%snapchat.com%',
          '%instagram.com%',
        ],
      ],
      'videos' => [
        'label' => 'WYSIWYG Video',
        'strings' => [
          '%<iframe%src="%youtube%"%',
          '%<iframe%src="%youtu.be%"%',
          '%<iframe%src="%vimeo%"%',
          '%<iframe%src="%brightcove%"%',
          '%<iframe%src="%facebook%"%',
          '%<iframe%src="%livestream%"%',
          '%<iframe%src="%cdn.jwplayer.com%"%',
          '%<iframe%src="%player.pbs.org%"%',
        ],
      ],
      'individual_files' => [
        'label' => 'WYSIWYG Individual Files',
        'strings' => [
          '%<a%href="%.pdf"%>%</a>%',
        ],
      ],
      'file_lists' => [],
    ];
    // Table prefixes.
    $this->stringQueriesTablePrefixes = [
      'data' => 'field_data_',
      'revision' => 'field_revision_',
    ];
    // Date columns.
    $this->dateColumns = [
      'created' => 'Creation Date',
      'changed' => 'Changed Date',
      'stamp' => 'Stamp Date',
    ];
    $this->metatagQueries = [
      'description' => [
        'label' => 'Metatag Description',
        'strings' => [
          '%description%',
        ],
      ],
      'abstract' => [
        'label' => 'Metatag Abstract',
        'strings' => [
          '%abstract%',
        ],
      ],
      'token' => [
        'label' => 'Metatag Token',
        'strings' => [
          '%[%:%]%',
        ],
      ],
      'creator' => [
        'label' => 'Metatag Creator',
        'strings' => [
          '%creator%',
        ],
      ],
      'canonical' => [
        'label' => 'Metatag Canonical',
        'strings' => [
          '%canonical%',
        ],
      ],
      'title' => [
        'label' => 'Metatag Title',
        'strings' => [
          '%title%',
        ],
      ],
      'image' => [
        'label' => 'Metatag Image',
        'strings' => [
          '%image%',
        ],
      ],
      'keyword' => [
        'label' => 'Metatag Keyword',
        'strings' => [
          '%keywords%',
        ],
      ],
      'video' => [
        'label' => 'Metatag Video',
        'strings' => [
          '%video%',
        ],
      ],
      'twitter' => [
        'label' => 'Metatag Twitter',
        'strings' => [
          '%twitter%',
        ],
      ],
      'og' => [
        'label' => 'Metatag Open Graph',
        'strings' => [
          '%og:%',
        ],
      ],
    ];
  }

  /**
   * Generates a report for a specified Drush site alias related database.
   *
   * @command squealer:squeal
   * @aliases squeal
   * @option rev Whether or not the revision tables should be scanned.
   * @option scan Run specific query types in a comma separated list
   *   (strq,dateq,parent,unpub,docs,paragraphs,field_collections,metaq,redirects,fieldq).
   * @option format Export as specific formats in a comma separated list (csv,excel).
   * @option strq Run a specific strq query or queries in a comma separated list.
   * @option dateq Run a specific dateq query or queries in a comma separated list.
   * @option metaq Run a specific metaq query or queries in a comma separated list.
   * @usage squealer:squeal
   *   Run scans and generates CSV files from the results.
   */
  public function squeal(
    array $options = [
      'rev' => FALSE,
      'scan' => '',
      'format' => 'excel',
      'strq' => '',
      'dateq' => '',
      'metaq' => '',
      'fieldq' => '',
    ]) {
    // Set the connection to the Drupal 7 database host.
    $this->connection = Database::getConnection('default', 'drupal_7');
    // Retrieve the paragraphs bundles from the database.
    $this->paragraphsBundles = $this->getParagraphsBundles();
    // Retrieve the paragraphs field names from the database.
    $this->paragraphsQueryFieldNames = $this->getParagraphsQueryFieldNames();
    // Retrieve the text field names from the database.
    $this->stringQueriesFieldNames = $this->getStringQueriesFieldNames();
    // If revisions are turned off, don't scan the revision tables.
    if (!$options['rev']) {
      unset($this->stringQueriesTablePrefixes['revision']);
    }
    // @todo: Consider moving this code to a separate validate method.
    // If no format is selected, default to Excel.
    if (!$options['format']) {
      $this->formats = ['excel'];
    }
    // Otherwise, verify the format provided is an option and use that format.
    else {
      $formats = [];
      $format_array = explode(',', $options['format']);
      foreach ($format_array as $format) {
        $format = trim($format);
        if (in_array($format, $this->formats)) {
          $formats[] = $format;
        }
        else {
          $this->logger()
            ->log(LogLevel::ERROR,
              dt('Could not identify the format: ' . $format . '.'));
        }
      }
      $this->formats = $formats;
    }
    // If scans are specified, only run those.
    if (!empty($options['scan'])) {
      $scans = [];
      $scan_array = explode(',', $options['scan']);
      foreach ($scan_array as $scan) {
        $scan = trim($scan);
        if (in_array($scan, $this->scans)) {
          $scans[] = $scan;
        }
        else {
          $this->logger()
            ->log(LogLevel::ERROR,
              dt('Could not identify the scan: ' . $scan . '.'));
        }
      }
      $this->scans = $scans;
    }
    // If string queries are specified, only run those.
    if (!empty($options['strq'])) {
      if (!in_array('strq', $this->scans)) {
        $this->scans[] = 'strq';
      }
      $string_queries = [];
      $strq_array = explode(',', $options['strq']);
      foreach ($strq_array as $string_query) {
        $string_query = trim($string_query);
        if (isset($this->stringQueries[$string_query])) {
          $string_queries[$string_query] = $this->stringQueries[$string_query];
        }
        else {
          $this->logger()
            ->log(LogLevel::ERROR,
              dt('Could not identify the string query: ' . $string_query . '.'));
        }
      }
      $this->stringQueries = $string_queries;
    }
    // If content date queries are specified, only run those.
    if ($options['dateq']) {
      if (!in_array('dateq', $this->scans)) {
        $this->scans[] = 'dateq';
      }
      $date_queries = [];
      $dateq_array = explode(',', $options['dateq']);
      foreach ($dateq_array as $date_query) {
        $date_query = trim($date_query);
        if (isset($this->contentDateQueries[$date_query])) {
          $date_queries[$date_query] = $this->contentDateQueries[$date_query];
        }
        else {
          $this->logger()
            ->log(LogLevel::ERROR,
              dt('Could not identify the content date query: ' . $date_query . '.'));
        }
      }
      $this->contentDateQueries = $date_queries;
    }
    // If metatag queries are specified, only run those.
    if (!empty($options['metaq'])) {
      if (!in_array('metaq', $this->scans)) {
        $this->scans[] = 'metaq';
      }
      $metatag_queries = [];
      $metaq_array = explode(',', $options['metaq']);
      foreach ($metaq_array as $metatag_query) {
        $metatag_query = trim($metatag_query);
        if (isset($this->metatagQueries[$metatag_query])) {
          $metatag_queries[$metatag_query] = $this->metatagQueries[$metatag_query];
        }
        else {
          $this->logger()
            ->log(LogLevel::ERROR,
              dt('Could not identify the metatag query: ' . $metatag_query . '.'));
        }
      }
      $this->metatagQueries = $metatag_queries;
    }

    // If field queries are specified, only run those.
    if (!empty($options['fieldq'])) {
      if (!in_array('fieldq', $this->scans)) {
        $this->scans[] = 'fieldq';
      }
      $field_queries = [];
      $fieldq_array = explode(',', $options['fieldq']);
      foreach ($fieldq_array as $field_query) {
        $field_query = trim($field_query);
        if (isset($this->fieldDataQueries[$field_query])) {
          $field_queries[$field_query] = $this->fieldDataQueries[$field_query];
        }
        else {
          $this->logger()
            ->log(LogLevel::ERROR,
              dt('Could not identify the field data query: ' . $field_query . '.'));
        }
      }
      $this->fieldDataQueries = $field_queries;
    }

    // Run Text Scans on field tables.
    if (in_array('strq', $this->scans) && !empty($this->stringQueries)) {
      foreach ($this->stringQueriesFieldNames as $field_name => $table_parameters) {
        foreach ($this->stringQueriesTablePrefixes as $table_prefix) {
          // Set variables for queries.
          $table_name = $table_prefix . $field_name;
          $column_name = $field_name . '_value';
          // Add related tables.
          $join_tables = [];
          if (!empty($table_parameters)) {
            $join_tables = $table_parameters['join_tables'];
          }
          // Run all string queries.
          foreach ($this->stringQueries as $query_name => $parameters) {
            if ($query_name == 'file_lists') {
              // File lists query.
              $this->queryFileLists($table_name, $column_name);
            }
            else {
              $this->queryStrings($table_name, $column_name, $parameters, $join_tables);
            }
          }
        }
      }
    }

    // Run Content Date Scans on the database.
    if (in_array('dateq', $this->scans) && !empty($this->contentDateQueries)) {
      foreach ($this->contentDateQueries as $parameters) {
        $this->queryContentDate($parameters);
      }
    }

    // Unpublished Content query.
    if (in_array('unpub', $this->scans)) {
      $this->queryUnpublishedContent();
    }

    // Paragraphs query.
    if (in_array('paragraphs', $this->scans)) {
      $this->queryParagraphs();
    }

    // Field Collections query.
//    if (in_array('field_collections', $this->scans)) {
//      $this->queryFieldCollections();
//    }

    // Run Metatag Scans.
    if (in_array('metaq', $this->scans) && !empty($this->metatagQueries)) {
      // Run all metatag queries.
      foreach ($this->metatagQueries as $query_name => $parameters) {
        $this->queryMetatags($parameters);
      }
    }

    // Run Field Data Scans.
    if (in_array('fieldq', $this->scans) && !empty($this->fieldDataQueries)) {
      // Run all field data queries.
      foreach ($this->fieldDataQueries as $field_name => $parameters) {
        foreach ($this->stringQueriesTablePrefixes as $table_prefix) {
          // Set variables for queries.
          $table_name = $table_prefix . $field_name;
          $this->queryFieldData($table_name, $parameters);
        }
      }
    }

    // Redirects query.
    if (in_array('redirects', $this->scans)) {
      $this->queryRedirects('other');
      $this->queryRedirects('node');
      $this->queryRedirects('taxonomy');
    }

    // Generate CSV reports.
    if (in_array('csv', $this->formats)) {
      $this->generateReportsCsv();
    }

    // Generate Excel reports.
    if (in_array('excel', $this->formats)) {
      $this->generateReportsExcel();
    }
  }

  /**
   * Helper function to retrieve paragraphs bundles.
   */
  private function getParagraphsBundles() {
    $bundles = [];
    // Get a list of text fields.
    $query = $this->connection->select('paragraphs_bundle', 'pb');
    $query->fields('pb', [
      'bundle',
    ]);
    $result = $query->distinct()->execute();
    foreach ($result as $row) {
      $bundles[$row->bundle] = [];
    }
    return $bundles;
  }

  /**
   * Helper function to retrieve the field names of paragraphs fields.
   */
  private function getParagraphsQueryFieldNames() {
    $field_names = [];
    // Get a list of text fields.
    $query = $this->connection->select('field_config', 'fc');
    $query->fields('fc', [
      'id',
      'field_name',
      'type',
    ]);
    $query->condition('type', '%paragraphs%', 'LIKE');
    $query->leftJoin(
      'field_config_instance',
      'fci',
      "fc.id = fci.field_id"
    );
    $query->fields('fci', [
      'entity_type',
      'bundle',
      'data',
    ]);
    $result = $query->distinct()->execute();
    foreach ($result as $row) {
      $field_name = $row->field_name;
      $field_data = unserialize($row->data);
      if (!in_array($row->entity_type, ['node', 'paragraphs_item'])) {
        continue;
      }
      $table_info = [
        'field_name' => $field_name,
        'prefix' => hash('crc32', $field_name, FALSE),
        'entity_types' => [
          'from' => 'paragraphs_item',
          'to' => 'node',
        ],
      ];
      $field_names[] = $table_info;
      if ($row->entity_type == 'paragraphs_item') {
        $this->logger()
          ->log(LogLevel::NOTICE,
            dt('Need to handle logic for nested paragraphs in getParagraphsQueryFieldNames().'));
      }
      foreach ($field_data['settings']['allowed_bundles'] as $bundle => $value) {
        if ($bundle == $value) {
          $this->paragraphsBundles[$bundle][$field_name] = $field_name;
        }
      }
    }
    return $field_names;
  }

  /**
   * Helper function to retrieve the field names of text fields.
   */
  private function getStringQueriesFieldNames() {
    $field_names = [];
    // Get a list of text fields.
    $query = $this->connection->select('field_config', 'fc');
    $query->fields('fc', [
      'id',
      'field_name',
      'type',
    ]);
    $query->condition('type', '%text%', 'LIKE');
    $query->leftJoin(
      'field_config_instance',
      'fci',
      "fc.id = fci.field_id"
    );
    $query->fields('fci', [
      'entity_type',
      'bundle',
    ]);
    $query->orderBy('entity_type');
    $result = $query->distinct()->execute();
    foreach ($result as $row) {
      $field_name = $row->field_name;
      if (!in_array($row->entity_type, ['node', 'paragraphs_item'])) {
        continue;
      }
      if (!isset($field_names[$field_name])) {
        $field_names[$field_name] = [];
      }
      if ($row->entity_type == 'paragraphs_item') {
        $paragraphs_bundle_fields = $this->paragraphsBundles[$row->bundle];
        foreach ($paragraphs_bundle_fields as $paragraphs_field_name) {
          // If the field name has already been added to joined_tables, skip it.
          if (isset($field_names[$field_name]['join_tables'])) {
            $field_joined = array_search($paragraphs_field_name, array_column($field_names[$field_name]['join_tables'], 'field_name'));
            if ($field_joined !== NULL) {
              continue;
            }
          }
          // Ensure that the joining field doesn't reflected nested paragraphs.
          if (empty($this->paragraphsQueryFieldNames[$paragraphs_field_name])) {
            $table_info = [
              'field_name' => $paragraphs_field_name,
              'prefix' => hash('crc32', $paragraphs_field_name, FALSE),
              'entity_types' => [
                'from' => 'paragraphs_item',
                'to' => 'node',
              ],
            ];
            $field_names[$field_name]['join_tables'][] = $table_info;
          }
          else {
            $this->logger()
              ->log(LogLevel::NOTICE,
                dt('Need to handle logic for nested paragraphs in getStringQueriesFieldNames().'));
          }
        }
      }
    }
    return $field_names;
  }

  /**
   * Helper function to retrieve the field names of date fields.
   */
  private function getDateFieldNames() {
    $field_names = [];
    // Get a list of text fields.
    $query = $this->connection->select('field_config', 'fc');
    $query->fields('fc', [
      'id',
      'field_name',
      'type',
    ]);
    $query->condition('type', '%date%', 'LIKE');
    $query->leftJoin(
      'field_config_instance',
      'fci',
      "fc.id = fci.field_id"
    );
    $query->fields('fci', [
      'entity_type',
      'bundle',
    ]);
    $query->orderBy('entity_type');
    $result = $query->distinct()->execute();
    foreach ($result as $row) {
      $field_name = $row->field_name;
      if (!in_array($row->entity_type, ['node', 'paragraphs_item'])) {
        continue;
      }
      if (!isset($field_names[$field_name])) {
        $field_names[$field_name] = [];
      }
      if ($row->entity_type == 'paragraphs_item') {
        $paragraphs_bundle_fields = $this->paragraphsBundles[$row->bundle];
        foreach ($paragraphs_bundle_fields as $paragraphs_field_name) {
          // If the field name has already been added to joined_tables, skip it.
          if (isset($field_names[$field_name]['join_tables'])) {
            $field_joined = array_search($paragraphs_field_name, array_column($field_names[$field_name]['join_tables'], 'field_name'));
            if ($field_joined !== NULL) {
              continue;
            }
          }
          // Ensure that the joining field doesn't reflected nested paragraphs.
          if (empty($this->paragraphsQueryFieldNames[$paragraphs_field_name])) {
            $table_info = [
              'field_name' => $paragraphs_field_name,
              'prefix' => hash('crc32', $paragraphs_field_name, FALSE),
              'entity_types' => [
                'from' => 'paragraphs_item',
                'to' => 'node',
              ],
            ];
            $field_names[$field_name]['join_tables'][] = $table_info;
          }
          else {
            $this->logger()
              ->log(LogLevel::NOTICE,
                dt('Need to handle logic for nested paragraphs in getStringQueriesFieldNames().'));
          }
        }
      }
    }
    return $field_names;
  }

  /**
   * Helper function to query for strings.
   *
   * @param string $table_name
   *   The database table name.
   * @param string $column_name
   *   The table column name.
   * @param array $parameters
   *   The string query parameters. Includes a label and a list of strings to
   *   query.
   * @param array $join_tables
   *   Tables to join for non-node entities.
   */
  private function queryStrings($table_name, $column_name, array $parameters, array $join_tables) {
    // Create a select query from the specified table.
    $query = $this->connection->select($table_name, 't');
    $query->fields('t', [
      'entity_id',
      'revision_id',
      'entity_type',
      'bundle',
      $column_name,
    ]);
    // Create orConditionGroup to ensure at least one nid value is not null.
    $group = $query->orConditionGroup();
    // Join additional tables to identify node.
    if (!empty($join_tables)) {
      $original_entity_type = $join_tables[0]['entity_types']['from'];
      foreach ($join_tables as $join_table_parameters) {
        $this->queryRecursiveJoin($query, $original_entity_type, 't', $join_table_parameters, $group);
      }
    }

    // Join the node and url_alias tables to provide alias links for nodes.
    $query->leftJoin(
      'node',
      'n',
      "t.entity_type = 'node' AND n.nid = t.entity_id"
    );
    $query->leftJoin(
      'url_alias',
      'u',
      "concat('node/', n.nid) = u.source"
    );
    // Add the fields to include in results.
    $query
      ->fields('n', ['title', 'created', 'status', 'nid', 'vid', 'type'])
      ->fields('u', ['alias']);
    // Add a condition to the orConditionGroup.
    $group->isNotNull("n.nid");

    // Add condition based on the number of strings.
    if (count($parameters['strings']) > 1) {
      $group_strings = $query->orConditionGroup();
      foreach ($parameters['strings'] as $string) {
        $group_strings->condition('t.' . $column_name, $string, 'LIKE');
      }
      $query->condition($group_strings);
    }
    else {
      $query->condition('t.' . $column_name, $parameters['strings'][0], 'LIKE');
    }
    $query->condition($group);

    // Execute the query.
    $result = $query->distinct()->execute();
    // Get the number of results.
    $result_count = $query->countQuery()->execute()->fetchField();

    if ($result) {
      $this->formatResults($result_count, $result, $parameters['label'], $table_name);
    }
  }

  /**
   * Helper function to query for content in relation to given date.
   *
   * @param array $parameters
   *   The string query parameters.
   */
  private function queryContentDate(array $parameters) {
    $query = $this->connection->select('node', 'n');
    $query->leftJoin('url_alias', 'u', "concat('node/', n.nid) = u.source");
    $expression = "from_unixtime(n." . $parameters['date_column'] . ")";
    $query->addExpression($expression, 'date');
    $query
      ->fields('n', [
        'nid',
        'type',
        'title',
        'status',
        $parameters['date_column'],
      ])
      ->fields('u', ['alias'])
      ->groupBy('nid')
      ->groupBy('type')
      ->groupBy('title')
      ->groupBy('status')
      ->groupBy($parameters['date_column'])
      ->groupBy('alias')
      ->condition('n.type', $parameters['content_type']);
    $query
      ->having($expression . " " . $parameters['operator'] . " :date", [
        ':date' => $parameters['date'],
      ]);

    $result = $query->distinct()->execute();
    $result_count = $query->countQuery()->execute()->fetchField();

    if ($result) {
      $this->formatResults($result_count, $result, $parameters['label']);
    }
  }

  /**
   * Helper function to query for file lists.
   *
   * @param string $table_name
   *   The database table name.
   * @param string $column_name
   *   The table column name.
   */
  private function queryFileLists($table_name, $column_name) {
    $query = $this->connection->select($table_name, 't');
    $query->join('node', 'n', 'n.nid = t.entity_id');
    $query->leftJoin('url_alias', 'u', "concat('node/', n.nid) = u.source");
    $expression = "round((length(:column_name) - length(replace(:column_name, '.pdf', ''))) / length('.pdf'))";
    $query->addExpression($expression, 'count', [
      ':column_name' => $column_name,
    ]);
    $query
      ->fields('t', ['entity_id', 'entity_type', 'bundle', $column_name])
      ->fields('n', ['title', 'created', 'status', 'nid', 'vid'])
      ->fields('u', ['alias'])
      ->having($expression . " > :count", [
        ':column_name' => $column_name,
        ':count' => 3,
      ]);

    $result = $query->distinct()->execute();
    $result_count = $query->countQuery()->execute()->fetchField();

    if ($result) {
      $this->formatResults($result_count, $result, 'WYSIWYG File Lists', $table_name);
    }
  }

  /**
   * Helper function to query for unpublished content.
   */
  private function queryUnpublishedContent() {
    $query = $this->connection->select('node', 'n');
    $query->leftJoin('url_alias', 'u', "concat('node/', n.nid) = u.source");
    $expression = "from_unixtime(n.changed)";
    $query->addExpression($expression, 'date');
    $query
      ->fields('n', ['nid', 'type', 'title', 'status', 'vid'])
      ->fields('u', ['alias'])
      ->orderBy('n.status')
      ->orderBy('n.nid')
      ->condition('n.status', 0);

    $result = $query->distinct()->execute();
    $result_count = $query->countQuery()->execute()->fetchField();

    if ($result) {
      $this->formatResults($result_count, $result, 'Unpublished Content');
    }
  }

  /**
   * Helper function to query for strings.
   *
   * @param string $table_name
   *   The database table name.
   * @param string $column_name
   *   The table column name.
   */
  private function queryParentContent($table_name, $column_name) {
    // Create a select query from the node table.
    $query = $this->connection->select('node', 'n');
    // Join the url_alias tables to provide alias links.
    $query->leftJoin('url_alias', 'u', "concat('node/', n.nid) = u.source");
    // Join the specified table.
    $query->join($table_name, 't', 'n.nid = t.entity_id');
    // Add the fields to include in results.
    $query
      ->fields('t', ['entity_id', 'entity_type', 'bundle', $column_name])
      ->fields('n', ['title', 'created', 'status', 'nid', 'vid'])
      ->fields('u', ['alias']);

    // Execute the query.
    $result = $query->distinct()->execute();
    // Get the number of results.
    $result_count = $query->countQuery()->execute()->fetchField();

    if ($result) {
      $this->formatResults($result_count, $result, 'Parent Content', $table_name);
    }
  }

  /**
   * Helper function to query for nodes with documents.
   *
   * @param string $table_name
   *   The database table name.
   * @param string $type
   *   The type of query to run. Options include:
   *   - multiple.
   *   - single_with_content.
   */
  private function queryDocuments($table_name, $type = 'multiple') {
    // Create a select query from the node table.
    $query = $this->connection->select('node', 'n');
    // Join the url_alias tables to provide alias links.
    $query->leftJoin('url_alias', 'u', "concat('node/', n.nid) = u.source");
    // Join the specified table.
    $query->join($table_name, 't', 'n.nid = t.entity_id');
    $expression = "count(*)";
    $query->addExpression($expression, 'count');
    // Add the fields to include in results.
    $query
      ->fields('t', ['entity_id', 'entity_type', 'bundle', 'revision_id'])
      ->fields('n', ['title', 'created', 'status', 'nid', 'vid'])
      ->fields('u', ['alias']);

    switch ($type) {
      case 'multiple':
        $label = 'Multiple';
        $query->having($expression . " > :count", [':count' => 1]);
        break;

      case 'single_with_content':
        $label = 'Single with Content';
        $query->having($expression . " = :count", [':count' => 1]);
        $query->leftJoin('field_data_body', 'b', 'n.nid = b.entity_id');
        $query->condition('b.body_value', NULL, 'IS NOT NULL');
        $query->condition('b.body_value', '', '>');
        break;
    }

    $query->groupBy('t.entity_type');
    $query->groupBy('t.bundle');
    $query->groupBy('t.entity_id');
    $query->groupBy('t.revision_id');
    $query->groupBy('n.title');
    $query->groupBy('n.created');
    $query->groupBy('n.status');
    $query->groupBy('n.nid');
    $query->groupBy('n.vid');
    $query->groupBy('u.alias');

    // Execute the query.
    $result = $query->distinct()->execute();
    // Get the number of results.
    $result_count = $query->countQuery()->execute()->fetchField();

    if ($result) {
      $this->formatResults($result_count, $result, $label, $table_name);
    }
  }

  /**
   * Helper function to query for paragraphs.
   */
  private function queryParagraphs() {
    $query = $this->connection->select('paragraphs_item', 'pi');
    // Create orConditionGroup to ensure at least one nid value is not null.
    $group = $query->orConditionGroup();
    // Join additional tables to identify node.
    foreach ($this->paragraphsQueryFieldNames as $field_parameters) {
      $table_name = $this->stringQueriesTablePrefixes['data'] . $field_parameters['field_name'];
      $column_name = $field_parameters['field_name'] . '_value';
      $prefix = $field_parameters['prefix'];
      // Join the field data table.
      $query->leftJoin(
        $table_name,
        $prefix,
         "pi.item_id = " . $prefix . "." . $column_name);
      // Add the fields for this table.
      $query->fields(
        $prefix,
        [$column_name, 'entity_id', 'entity_type', 'revision_id']
      );
      // Join additional tables to identify node.
      if (isset($field_parameters['join_tables'])) {
        foreach ($field_parameters['join_tables'] as $join_field_parameters) {
          // Add the prefix to the recursive table list.
          $this->recursiveNodePrefixes[$prefix] = $prefix;
          // Join related tables.
          $this->queryRecursiveJoin($query, 'paragraphs_item', $prefix, $join_field_parameters, $group);
        }
      }
      else {
        // Join the node table to associate to nodes.
        $query->leftJoin(
          'node',
          $prefix . 'n',
          $prefix . ".entity_type = 'node'
        AND " . $prefix . "n.nid = " . $prefix . ".entity_id"
        );
        // Add URL aliases.
        $query->leftJoin(
          'url_alias',
          $prefix . 'u',
          "concat('node/', " . $prefix . "n.nid) = " . $prefix . "u.source"
        );
        $query
          ->fields(
            $prefix . 'n',
            ['title', 'created', 'status', 'nid', 'vid', 'type']
          )
          ->fields($prefix . 'u', ['alias']);
        // Add a condition to the orConditionGroup.
        $group->isNotNull($prefix . "n.nid");
      }
    }
    // Add the fields to include in results.
    $query->fields('pi', ['bundle', 'field_name']);
    // Add orConditionGroup to ensure at least one nid value is not null.
    $query->condition($group);

    // Execute the query.
    $result = $query->distinct()->execute();
    // Get the number of results.
    $result_count = $query->countQuery()->execute()->fetchField();

    if ($result) {
      $this->formatResults($result_count, $result, 'Paragraphs');
    }
  }

  /**
   * Helper function to query for field collections.
   */
  private function queryFieldCollections() {
    // Query to retrieve the field_collection fields used for the site.
    $field_query = $this->connection->select('field_collection_item', 'fci');
    $field_query->fields('fci', ['field_name']);
    $field_result = $field_query->distinct()->execute()->fetchAllKeyed();
    // Check if the site is using field_collection fields.
    if (!empty($field_result)) {
      // Grab the field_names from the result keys.
      $field_names = array_keys($field_result);
      // Start main query.
      $query = $this->connection->select('field_collection_item', 'fci');
      // Create orConditionGroup to ensure at least one nid value is not null.
      $group = $query->orConditionGroup();
      // Join additional tables to identify node.
      foreach ($field_names as $field_name) {
        $table_name = $this->stringQueriesTablePrefixes['data'] . $field_name;
        $column_name = $field_name . '_value';
        $prefix = $table_name;
        // Join the field data table.
        $query->leftJoin(
          $table_name,
          $prefix,
          "fci.item_id = " . $prefix . "." . $column_name);
        // Add the fields for this table.
        $query->fields(
          $prefix,
          [$column_name, 'entity_id', 'entity_type', 'revision_id']
        );
        // Join the node table to associate to nodes.
        $query->leftJoin(
          'node',
          $prefix . 'n',
          $prefix . ".entity_type = 'node'
        AND " . $prefix . "n.nid = " . $prefix . ".entity_id"
        );
        // Add URL aliases.
        $query->leftJoin(
          'url_alias',
          $prefix . 'u',
          "concat('node/', " . $prefix . "n.nid) = " . $prefix . "u.source"
        );
        $query
          ->fields(
            $prefix . 'n',
            ['title', 'created', 'status', 'nid', 'vid', 'type']
          )
          ->fields($prefix . 'u', ['alias']);
        // Add a condition to the orConditionGroup.
        $group->isNotNull($prefix . "n.nid");
        // Add prefix to node table prefixes array.
        $this->recursiveNodePrefixes[$prefix] = $prefix;
      }
      // Add the fields to include in results.
      $query->fields('fci', ['field_name', 'item_id']);
      // Add orConditionGroup to ensure at least one nid value is not null.
      $query->condition($group);

      // Execute the query.
      $result = $query->distinct()->execute();
      // Get the number of results.
      $result_count = $query->countQuery()->execute()->fetchField();

      if ($result) {
        $this->formatResults($result_count, $result, 'Field Collections');
      }
    }
  }

  /**
   * Helper function to query for metatag overrides.
   *
   * @param array $parameters
   *   The string query parameters. Includes a label and a list of strings to
   *   query.
   */
  private function queryMetatags(array $parameters) {
    // Create a select query from the metatag table.
    $query = $this->connection->select('metatag', 'm');
    $query->fields('m', [
      'entity_type',
      'entity_id',
      'data',
      'revision_id',
    ]);
    // Join the node and url_alias tables to provide alias links for nodes.
    $query->leftJoin(
      'node',
      'n',
      "m.entity_type = 'node'
      AND n.nid = m.entity_id
      AND n.vid = m.revision_id"
    );
    $query->leftJoin(
      'url_alias',
      'u',
      "concat('node/', n.nid) = u.source"
    );
    // Add the fields to include in results.
    $query
      ->fields('n', ['title', 'created', 'status', 'nid', 'vid', 'type'])
      ->fields('u', ['alias']);

    // Add condition based on the number of strings.
    if (count($parameters['strings']) > 1) {
      $group_strings = $query->orConditionGroup();
      foreach ($parameters['strings'] as $string) {
        $group_strings->condition('m.data', $string, 'LIKE');
      }
      $query->condition($group_strings);
    }
    else {
      $query->condition('m.data', $parameters['strings'][0], 'LIKE');
    }
    // Ensure nodes are identified.
    $query->isNotNull("n.nid");

    // Execute the query.
    $result = $query->distinct()->execute();
    // Get the number of results.
    $result_count = $query->countQuery()->execute()->fetchField();

    if ($result) {
      $this->formatResults($result_count, $result, $parameters['label']);
    }
  }

  /**
   * Helper function to query for field values.
   *
   * @param string $table_name
   *   The name of the field table.
   * @param array $parameters
   *   The query parameters. Includes a label and a list of field value fields.
   */
  private function queryFieldData($table_name, array $parameters) {
    // Set the value_columns as $headers.
    $headers = $parameters['value_columns'];
    // Add the standard columns to the value_columns for the query fields array.
    $parameters['value_columns'][] = 'entity_id';
    $parameters['value_columns'][] = 'revision_id';
    $parameters['value_columns'][] = 'entity_type';
    $parameters['value_columns'][] = 'bundle';
    // Create a select query from the specified table.
    $query = $this->connection->select($table_name, 't');
    $query->fields('t', $parameters['value_columns']);

    // Join the node and url_alias tables to provide alias links for nodes.
    $query->leftJoin(
      'node',
      'n',
      "t.entity_type = 'node'
      AND n.nid = t.entity_id
      AND n.vid = t.revision_id"
    );
    $query->leftJoin(
      'url_alias',
      'u',
      "concat('node/', n.nid) = u.source"
    );
    // Add the fields to include in results.
    $query
      ->fields('n', ['title', 'created', 'status', 'nid', 'vid', 'type'])
      ->fields('u', ['alias']);
    // Add a condition to the query.
    $query->isNotNull("n.nid");

    // Execute the query.
    $result = $query->distinct()->execute();
    // Get the number of results.
    $result_count = $query->countQuery()->execute()->fetchField();

    if ($result) {
      $this->formatResults($result_count, $result, $parameters['label'], $table_name, $headers);
    }
  }

  /**
   * Helper function to query for redirects.
   */
  private function queryRedirects($type = 'other') {
    $query = $this->connection->select('redirect', 'r');

    // Add the fields to include in results.
    $query->fields('r', ['source']);
    $query->fields('r', ['redirect']);
    $query->orderBy('r.redirect');

    switch ($type) {
      case 'node':
        $query->condition('r.redirect', "node/%", 'LIKE');
        break;

      case 'taxonomy':
        $query->condition('r.redirect', "taxonomy/term/%", 'LIKE');
        break;

      case 'other':
        $query->condition('r.redirect', "node/%", 'NOT LIKE');
        $query->condition('r.redirect', "taxonomy/term/%", 'NOT LIKE');
        break;

    }

    // Execute the query.
    $result = $query->distinct()->execute();
    // Get the number of results.
    $result_count = $query->countQuery()->execute()->fetchField();

    if ($result) {
      $label = "Redirects $type";
      $headers = [
        'Redirect from',
        'Redirect from (alias)',
        'Redirect to',
        'Redirect to (alias)',
      ];
      $rows = [];
      if ($result_count > 0) {
        while ($row = $result->fetchAssoc()) {
          $data = [
            0 => $row['source'],
            1 => $this->sourceAliasToggle($row['source']),
            2 => $row['redirect'],
            3 => $this->sourceAliasToggle($row['redirect']),
          ];
          // Store data as a row.
          $rows[md5(serialize($data))] = $data;
        }
      }
      $this->formatResults($result_count, $result, $label, 'redirect', $headers, $rows);
    }
  }

  /**
   * Helper function to join a table to the previous table.
   *
   * This is used to join tables at different levels, so that nodes can be
   * identified from field_collection_item and paragraphs_item content. It is
   * a recursive function because the table joining varies.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query database connection.
   * @param string $original_entity_type
   *   The original entity type from the first field.
   * @param string $parent_prefix
   *   The table prefix for the previous table.
   * @param array $field_parameters
   *   The table parameters.
   * @param \Drupal\Core\Database\Query\Condition $group
   *   An orConditionGroup, if it exists.
   */
  private function queryRecursiveJoin(
    SelectInterface $query,
    $original_entity_type,
    $parent_prefix,
    array $field_parameters,
    Condition $group = NULL) {
    $table_name = $this->stringQueriesTablePrefixes['data'] . $field_parameters['field_name'];
    $column_name = $field_parameters['field_name'] . '_value';
    $prefix = $field_parameters['prefix'];
    $from_type = $field_parameters['entity_types']['from'];
    $to_type = $field_parameters['entity_types']['to'];
    // Join the related table.
    $query->leftJoin(
      $table_name,
      $prefix,
      $parent_prefix . ".entity_type = :type
        AND " . $prefix . "." . $column_name . " = " . $parent_prefix . ".entity_id",
      [
        ':type' => $from_type,
      ]
    );
    // Add the fields for this table.
    $query->fields(
      $prefix,
      [$column_name, 'entity_id', 'entity_type', 'revision_id']
    );
    if ($to_type == 'node') {
      $this->recursiveNodePrefixes[$prefix] = $prefix;
      // Join the node table to associate to nodes.
      $query->leftJoin(
        'node',
        $prefix . 'n',
        $prefix . ".entity_type = 'node'
        AND " . $prefix . "n.nid = " . $prefix . ".entity_id"
      );
      // Add URL aliases.
      $query->leftJoin(
        'url_alias',
        $prefix . 'u',
        "concat('node/', " . $prefix . "n.nid) = " . $prefix . "u.source"
      );
      $query
        ->fields(
          $prefix . 'n',
          ['title', 'created', 'status', 'nid', 'vid', 'type']
        )
        ->fields($prefix . 'u', ['alias']);
      if (!is_null($group)) {
        // Add a condition to the orConditionGroup.
        $group->isNotNull($prefix . "n.nid");
      }
    }
    // Recursively join other tables.
    if (isset($field_parameters['join_tables'])) {
      foreach ($field_parameters['join_tables'] as $join_field_parameters) {
        $this->queryRecursiveJoin($query, $original_entity_type, $prefix, $join_field_parameters, $group);
      }
    }
  }

  /**
   * Helper function to set the current row prefixes.
   *
   * @param string $report_type
   *   The report type key.
   * @param array $row
   *   The current row array.
   */
  private function setRowPrefixes($report_type, array $row) {
    // If the node table was joined with prefixed aliases, use the prefix
    // that has an nid value.
    // Initialize prefixes.
    $this->reports[$report_type]['field_prefix'] = '';
    $this->reports[$report_type]['node_prefix'] = '';
    $this->reports[$report_type]['url_alias_prefix'] = '';
    if (!empty($this->recursiveNodePrefixes)) {
      foreach ($this->recursiveNodePrefixes as $prefix) {
        if (isset($row[$prefix . 'n_nid'])
          && !is_null($row[$prefix . 'n_nid'])) {
          $this->reports[$report_type]['field_prefix'] = $prefix . '_';
          $this->reports[$report_type]['node_prefix'] = $prefix . 'n_';
          $this->reports[$report_type]['url_alias_prefix'] = $prefix . 'u_';
          break;
        }
      }
    }
  }

  /**
   * Helper function to prepare table headers.
   *
   * @param int $result_count
   *   Number of results.
   * @param array $result_rows
   *   Data for the report.
   * @param string $report_type
   *   Type of report.
   * @param string $table_name
   *   Name of the table.
   * @param array $value_columns
   *   Names of value_columns.
   */
  private function prepareHeaders($result_count,
                                 array $result_rows,
                                 $report_type,
                                 $table_name = '',
                                 array $value_columns = []) {
    // Set count to identify first row for headers.
    $count = 0;
    // Initialize date header.
    $date_key = '';
    // Initialize headers and rows arrays.
    $headers = [];
    if ($result_count > 0) {
      foreach ($result_rows as $row) {
        // Set default node alias and URL alias table prefix.
        $this->setRowPrefixes($report_type, $row);
        $field_prefix = $this->reports[$report_type]['field_prefix'];
        $node_prefix = $this->reports[$report_type]['node_prefix'];

        // Set headers based on first $row.
        if ($count == 0) {
          $count++;
          // Set headers to include correct date type.
          foreach ($this->dateColumns as $column => $label) {
            if (isset($row[$node_prefix . $column])
              && !is_null($row[$node_prefix . $column])) {
              $date_key = $column;
              break;
            }
          }
          // Set default headers.
          $headers = [
            'Node ID',
            'Title',
            'Status',
            'Revision/Current',
            'Entity Type',
            'Url Alias',
          ];
          if (!empty($value_columns)) {
            foreach ($value_columns as $column) {
              $headers[] = $column;
            }
          }
          // Add date header if it was identified.
          if (!empty($date_key)) {
            $headers[] = $this->dateColumns[$date_key];
          }
          // Add table name header if it exists.
          if (!empty($table_name)) {
            $headers[] = 'Table Name';
          }
          // Add bundle header if it exists.
          if (isset($row[$field_prefix . 'bundle'])) {
            $headers[] = 'Bundle';
          }
          elseif (isset($row[$node_prefix . 'type'])) {
            $headers[] = 'Bundle';
          }
          // Add field name header if it exists.
          if (isset($row['field_name'])) {
            $headers[] = 'Field Name';
          }
          break;
        }
      }
    }
    $this->reports[$report_type]['date_key'] = $date_key;

    return $headers;
  }

  /**
   * Helper function to prepare the rows.
   *
   * @param int $result_count
   *   Number of results.
   * @param array $result_rows
   *   Data for the report.
   * @param string $report_type
   *   Type of report.
   * @param string $table_name
   *   Name of the table.
   * @param array $value_columns
   *   Names of value_columns.
   */
  private function prepareRows($result_count,
                                 array $result_rows,
                                 $report_type,
                                 $table_name = '',
                                 array $value_columns = []) {

    $date_key = $this->reports[$report_type]['date_key'];

    $rows = [];
    if ($result_count > 0) {
      foreach ($result_rows as $row) {
        // Set row node alias and URL alias table prefix.
        $this->setRowPrefixes($report_type, $row);
        $field_prefix = $this->reports[$report_type]['field_prefix'];
        $node_prefix = $this->reports[$report_type]['node_prefix'];
        $url_alias_prefix = $this->reports[$report_type]['url_alias_prefix'];
        // Get correct date.
        $date = '';
        if (!empty($date_key)) {
          if ($row_date = $row[$node_prefix . $date_key]) {
            $date = $this->dateFormatter->format($row_date, 'html_date');
          }
        }
        // Ensure only nodes show a URL alias.
        $url_alias = '';
        $nid = '';
        if (isset($row[$node_prefix . 'nid'])) {
          $url_alias = $this->siteDomainUrl . $row[$url_alias_prefix . 'alias'];
          $nid = $row[$node_prefix . 'nid'];
        }
        // Set the status if provided.
        $status = '';
        if (isset($row[$node_prefix . 'status'])) {
          $status = 'Not Published';
          if ($row[$node_prefix . 'status'] == 1) {
            $status = 'Published';
          }
        }
        // Add Revision.
        $revision = '';
        if (isset($row[$node_prefix . 'vid'])
          && isset($row[$field_prefix . 'revision_id'])) {
          $revision = 'Current';
          if ($row[$node_prefix . 'nid'] == $row[$field_prefix . 'entity_id']
            && $row[$node_prefix . 'vid'] != $row[$field_prefix . 'revision_id']) {
            $status = '';
            $revision = 'Revision';
          }
        }
        // Add Entity Type.
        $entity_type = 'node';
        if (isset($row[$node_prefix . 'type']) && isset($row[$field_prefix . 'entity_type'])) {
          if ($row[$node_prefix . 'type'] != $row[$field_prefix . 'entity_type']) {
            $entity_type = $row[$field_prefix . 'entity_type'];
          }
        }
        // Set default data for CSV row.
        $data = [
          'Node ID' => $nid,
          'Title' => $row[$node_prefix . 'title'],
          'Status' => $status,
          'Revision/Current' => $revision,
          'Entity Type' => $entity_type,
          'Url Alias' => $url_alias,
        ];
        // Add data for field value columns, if provided.
        if (!empty($value_columns)) {
          foreach ($value_columns as $value_column) {
            $data[$value_column] = $row[$value_column];
          }
        }
        // Add date to correct header key if it exists.
        if (!empty($date_key)) {
          $data[$this->dateColumns[$date_key]] = $date;
        }
        // Add table name if it exists.
        if (!empty($table_name)) {
          $data['Table Name'] = $table_name;
        }
        // Add bundle if it exists.
        if (!empty($row[$field_prefix . 'bundle'])) {
          $data['Bundle'] = $row[$field_prefix . 'bundle'];
        }
        elseif (!empty($row[$node_prefix . 'type'])) {
          $data['Bundle'] = $row[$node_prefix . 'type'];
        }
        // Add field name header if it exists.
        if (isset($row['field_name'])) {
          $data['Field Name'] = $row['field_name'];
        }
        // Store data as a row.
        $rows[md5(serialize($data))] = $data;
      }
    }
    return $rows;
  }

  /**
   * Helper function to format the results.
   *
   * @param int $result_count
   *   Number of results.
   * @param \Drupal\Core\Database\StatementInterface $query_result
   *   Data for the report.
   * @param string $report_type
   *   Type of report.
   * @param string $table_name
   *   Name of the table.
   * @param array $headers
   *   Optional array of headers for this report. If not provided, headers
   *   will be constructed. Reports that use the normal headers should leave
   *   this value empty, it's provided to allow a report that has very
   *   different headers, such as the redirects report.
   * @param array $rows
   *   Optional array of rows for this report. If not provided, rows
   *   will be constructed. Reports that use the normal rows should leave
   *   this value empty, it's provided to allow a report that has very
   *   different rows, such as the redirects report.
   */
  private function formatResults($result_count,
                                 StatementInterface $query_result,
                                 $report_type,
                                 $table_name = '',
                                 array $headers = [],
                                 array $rows = []) {
    if ($result_count > 0) {
      if (empty($rows)) {
        $this->reports[$report_type]['date_key'] = '';
        $this->reports[$report_type]['field_prefix'] = '';
        $this->reports[$report_type]['node_prefix'] = '';
        $this->reports[$report_type]['url_alias_prefix'] = '';
        $value_columns = !empty($headers) ? $headers : [];
        $rows = $query_result->fetchAll(\PDO::FETCH_ASSOC);
        $headers = $this->prepareHeaders($result_count, $rows, $report_type, $table_name, $value_columns);
        $rows = $this->prepareRows($result_count, $rows, $report_type, $table_name, $value_columns);
      }
      if (!empty($rows)) {
        $this->reports[$report_type]['headers'] = $headers;
        // Store report results.
        $this->reports[$report_type]['tables'][$table_name] = [
          'rows' => $rows,
          'result_count' => $result_count,
        ];
      }
    }
    else {
      // Provided a Drush log message to identify reports without results.
      if (!empty($table_name)) {
        $this->logger()
          ->log(LogLevel::NOTICE,
            dt('There were no results for ' . $report_type . ' in table ' . $table_name . '.'));
      }
      else {
        $this->logger()
          ->log(LogLevel::NOTICE,
            dt('There were no results for ' . $report_type . '.'));
      }
    }
  }

  /**
   * Helper function to generate CSV files from report data.
   */
  private function generateReportsCsv() {
    if (count($this->reports) > 0) {
      foreach ($this->reports as $report_type => $report) {
        // Get current date.
        $date = date('Y-m-d');
        // Create a dated directory for the reports.
        $destination = 'public://squealer_reports/' . $date;
        file_prepare_directory($destination, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
        // Get the real path of the files that will be generated.
        $files_directory = $this->fileSystem->realpath('public://');
        $report_type = str_replace(' ', '_', $report_type);
        $file_name = $files_directory . '/squealer_reports/' . $date . '/' . $report_type . '.csv';
        // Create or open the report file.
        $fp = fopen($file_name, 'w');
        // Write the header row.
        fputcsv($fp, $report['headers']);
        // Loop through reports.
        foreach ($report['tables'] as $result) {
          // Loop through the result rows.
          foreach ($result['rows'] as $data) {
            // Write the result row to the CSV file.
            fputcsv($fp, $data);
          }
        }
        // Close the report file.
        fclose($fp);
        $this->logger()
          ->log(LogLevel::SUCCESS, dt('The following report was generated: ' . $file_name));
      }
    }
    else {
      $this->logger()
        ->log(LogLevel::NOTICE,
          dt('There were no reports generated.'));
    }
  }

  /**
   * Helper function to generate an Excel file from report data.
   */
  private function generateReportsExcel() {
    if (empty($this->reports)) {
      $this->logger->notice('There were no reports generated.');
      return;
    }
    $spreadsheet = new Spreadsheet();
    // Get current date.
    $date = date('Y-m-d');
    // Get the real path of the files that will be generated.
    $files_directory = $this->fileSystem->realpath('public://');
    $domain = str_replace('/', '', $this->sitePath);
    $file_name = $files_directory . '/squealer_reports/' . $domain . '_' . $date . '.xlsx';
    // Initialize the report counter.
    $report_index = 0;
    foreach ($this->reports as $report_type => $report) {
      // Create report.
      if ($report_index > 0) {
        $spreadsheet->createSheet();
      }
      // Set the active sheet to the current report index.
      $spreadsheet->setActiveSheetIndex($report_index);
      // Set the worksheet title.
      $report_type = str_replace(' ', '_', $report_type);
      $spreadsheet->getActiveSheet()->setTitle($report_type);
      $data_array = [$report['headers']];
      // Loop through reports and merge rows arrays to report data_array.
      foreach ($report['tables'] as $result) {
        $data_array = array_merge($data_array, $result['rows']);
      }
      // Write report data.
      $spreadsheet->getActiveSheet()
        ->fromArray($data_array, 'A1');
      // Set auto filter for the data range.
      $spreadsheet->getActiveSheet()
        ->setAutoFilter(
          $spreadsheet->getActiveSheet()
            ->calculateWorksheetDimension()
        );
      // Get the highest column with data.
      $highest_column = $spreadsheet->getActiveSheet()
        ->getHighestDataColumn();
      // Autosize the column widths.
      for ($col = 'A'; $col < $highest_column; $col++) {
        $spreadsheet->getActiveSheet()
          ->getColumnDimension($col)
          ->setAutoSize(TRUE);
      }
      // Freeze first row.
      $spreadsheet->getActiveSheet()
        ->freezePane('A2');
      // Bold first row.
      $spreadsheet->getActiveSheet()
        ->getStyle('A1:' . $highest_column . '1')
        ->getFont()
        ->setBold(TRUE);
      // Log the report worksheet.
      $this->logger()
        ->log(LogLevel::SUCCESS, dt('The following report was added: ' . $report_type));
      // Increment the report index.
      $report_index++;
    }
    // Set the active sheet to the first.
    $spreadsheet->setActiveSheetIndex(0);
    // Create the Excel file.
    $xlsx_writer = new Xlsx($spreadsheet);
    $xlsx_writer->save($file_name);
    // Log creation of the Excel file.
    $this->logger()
      ->log(LogLevel::SUCCESS, dt('The following report file was generated: ' . $file_name));
  }

  /**
   * Get an alias for a source and vice-versa.
   *
   * @param mixed $original
   *   A system path or alias.
   *
   * @return string
   *   This will return:
   *   - the alias for a source or the source for an alias.
   */
  private function sourceAliasToggle($original) {
    // Retrieve the alias from a source.
    $alias = $this->connection->select('url_alias', 'ua')
      ->fields('ua', ['alias'])
      ->condition('source', $original)
      ->execute()
      ->fetchField();
    if (empty($alias)) {
      // Retrieve the source from an alias.
      $query = $this->connection->select('url_alias', 'ua')
        ->fields('ua', ['source']);
      // Check with or without the leading '/'.
      $source_or_group = $query->orConditionGroup()
        ->condition('alias', $original)
        ->condition('alias', ltrim($original, '/'));
      $source = $query
        ->condition($source_or_group)
        ->execute()
        ->fetchField();
      if (empty($source)) {
        // Retrieve the source from a redirect.
        $query = $this->connection->select('redirect', 'r')
          ->fields('r', ['redirect']);
        // Check with or without the leading '/'.
        $redirect_or_group = $query->orConditionGroup()
          ->condition('source', $original)
          ->condition('source', ltrim($original, '/'));
        $redirect = $query
          ->condition($redirect_or_group)
          ->execute()
          ->fetchField();
        if (empty($redirect)) {
          return NULL;
        }
        return $redirect;
      }
      return $source;
    }
    return $alias;
  }

}
